<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/game")
 */
class GameController extends Controller
{
    /**
     * This action displays the main board to play the game.
     *
     * @Route("", name="app_game_play")
     * @Method("GET")
     */
    public function playAction()
    {
        return $this->render('game/board.html.twig');
    }

    /**
     * This action displays the congratulations page.
     *
     * @Route("/win", name="app_game_win")
     * @Method("GET")
     */
    public function winAction()
    {
        return $this->render('game/win.html.twig');
    }

    /**
     * This action displays the losing page.
     *
     * @Route("/fail", name="app_game_fail")
     * @Method("GET")
     */
    public function failAction()
    {
        return $this->render('game/fail.html.twig');
    }

    /**
     * This action resets the current game and starts a new one.
     *
     * @Route("/reset", name="app_game_reset")
     * @Method("GET")
     */
    public function resetAction()
    {
        return $this->redirectToRoute('app_game_play');
    }

    /**
     * This action tries one single letter at a time.
     *
     * @Route("/play/{letter}", name="app_game_play_letter", requirements={"letter"="[a-z]"})
     * @Method("GET")
     */
    public function playLetterAction($letter)
    {
        return $this->redirectToRoute('app_game_play');
    }

    /**
     * This action tries one single word at a time.
     *
     * @Route(
     *   path="/play",
     *   name="app_game_play_word",
     *   condition="request.request.get('word') matches '/^[a-z]+$/i'"
     * )
     * @Method("POST")
     */
    public function playWordAction()
    {
        return $this->redirectToRoute('app_game_play');
    }
}
